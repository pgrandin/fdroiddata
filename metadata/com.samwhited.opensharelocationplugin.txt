Categories:Internet
License:FreeBSD
Web Site:
Source Code:https://bitbucket.org/SamWhited/opensharelocationplugin
Issue Tracker:https://bitbucket.org/SamWhited/opensharelocationplugin/issues
Bitcoin:1PYd7Koqd3ucSxKQRZQZRoB3qi7WaAFvL5

Auto Name:Open Share Location Plugin
Summary:Easy share your location in Conversations
Description:
This plugin for [[eu.siacs.conversations]] allows you to easily share your
location in a chat.

Written by Sam Whited, see the
[https://bitbucket.org/SamWhited/opensharelocationplugin/src/HEAD/LICENSE
LICENSE file] for further information.
.

Repo Type:git
Repo:https://bitbucket.org/SamWhited/opensharelocationplugin.git

Build:1.1.1,3
    commit=v1.1.1
    gradle=yes

Build:1.1.2,4
    commit=v1.1.2
    gradle=yes

Build:1.1.3,5
    commit=v1.1.3
    gradle=yes

Build:1.2.0-beta3,6
    commit=v1.2.0-beta3
    gradle=yes

Archive Policy:0 versions
Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.0-beta3
Current Version Code:6

