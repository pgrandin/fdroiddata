Categories:Multimedia
License:GPLv3+
Web Site:
Source Code:https://github.com/Ph1b/MaterialAudiobookPlayer
Issue Tracker:https://github.com/Ph1b/MaterialAudiobookPlayer/issues
Changelog:https://github.com/Ph1b/MaterialAudiobookPlayer/blob/HEAD/CHANGELOG.md

Auto Name:Material Player
Summary:Simple audiobook player
Description:
Audiobook player with the following basic features:

* Remembers last position
* Easy, intuitive interface
* Sleep-timer
* Easily jump to a position
.

Repo Type:git
Repo:https://github.com/Ph1b/MaterialAudiobookPlayer

Build:1.2.1,5
    commit=348a47364b151f698c8
    subdir=audiobook
    gradle=yes

Build:1.2.2,6
    commit=1.2.2
    subdir=audiobook
    gradle=yes

Build:1.2.5.1,10
    commit=v1.2.5.1
    subdir=audiobook
    gradle=yes

Build:1.3.0,15
    commit=v1.3.0
    subdir=audiobook
    gradle=yes

Build:1.4.0,16
    commit=v1.4.0
    subdir=audiobook
    gradle=yes

Build:1.4.1,17
    commit=v1.4.1
    subdir=audiobook
    gradle=yes

Build:1.4.1.1,18
    commit=v1.4.1.1
    subdir=audiobook
    gradle=yes

Build:1.4.2,19
    commit=v1.4.2
    subdir=audiobook
    gradle=yes

Build:1.4.3,20
    commit=v1.4.3
    subdir=audiobook
    gradle=yes

Build:1.4.5,22
    commit=v1.4.5
    subdir=audiobook
    gradle=yes

Build:1.4.6,23
    commit=v1.4.6
    subdir=audiobook
    gradle=yes

Build:1.4.6.1,24
    commit=v1.4.6.1
    subdir=audiobook
    gradle=yes

Build:1.5.0,25
    disable=binaries, missing jni source
    commit=v1.5.0
    subdir=audiobook
    gradle=yes

Build:1.5.2,27
    disable=binaries, missing jni source
    commit=v1.5.2
    subdir=audiobook
    gradle=yes

Build:1.5.3,28
    commit=v1.5.3
    subdir=audiobook
    gradle=yes

Build:2.0-RC2,39
    commit=v2.0-fdroid
    subdir=audiobook
    gradle=yes

Build:2.0.3,43
    commit=v2.0.3
    subdir=audiobook
    gradle=yes

Build:2.0.4,44
    commit=v2.0.4
    subdir=audiobook
    gradle=yes

Build:2.0.5,45
    commit=v2.0.5
    subdir=audiobook
    gradle=yes

Build:2.0.7,47
    commit=v2.0.7
    subdir=audiobook
    gradle=yes

Build:2.0.8,48
    commit=v2.0.8
    subdir=audiobook
    gradle=yes

Build:2.0.9.1,50
    commit=v2.0.9.1
    subdir=audiobook
    gradle=yes

Build:2.1.0,51
    commit=v2.1.0
    subdir=audiobook
    gradle=yes

Build:2.1.1,52
    commit=v2.1.1
    subdir=audiobook
    gradle=yes

Build:2.1.2.1,54
    commit=v2.1.2.1
    subdir=audiobook
    gradle=yes

Build:2.1.3,56
    commit=v2.1.3
    subdir=audiobook
    gradle=yes

Build:2.1.5,59
    commit=v2.1.5
    subdir=audiobook
    gradle=yes

Build:2.1.6,60
    commit=v2.1.6
    subdir=audiobook
    gradle=yes

Build:2.1.6.1,61
    commit=v2.1.6.1
    subdir=audiobook
    gradle=yes

Build:2.1.7,62
    commit=v2.1.7
    subdir=audiobook
    gradle=yes

Build:2.1.8,63
    commit=v2.1.8
    subdir=audiobook
    gradle=yes

Build:2.1.9,64
    commit=v2.1.9
    subdir=audiobook
    gradle=yes

Build:2.1.9.1,65
    commit=v2.1.9.1
    subdir=audiobook
    gradle=yes

Build:2.2,66
    commit=v2.2
    subdir=audiobook
    gradle=yes

Build:2.2.1,67
    commit=v2.2.1
    subdir=audiobook
    gradle=yes

Build:2.2.2,68
    commit=v2.2.2
    subdir=audiobook
    gradle=yes

Build:2.2.3.1,70
    commit=v2.2.3.1
    subdir=audiobook
    gradle=yes

Build:2.3.0,71
    commit=v2.3.0
    subdir=audiobook
    gradle=yes
    forceversion=yes

Build:2.4.0,72
    commit=v2.4.0
    subdir=audiobook
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/applicationVariants.all/i/*' -e '/dependencies {/i*/\n\n}\n' build.gradle

Build:2.4.0.1,73
    commit=v2.4.0.1
    subdir=audiobook
    gradle=yes
    forceversion=yes
    prebuild=sed -i -e '/applicationVariants.all/i/*' -e '/dependencies {/i*/\n\n}\n' build.gradle

#Auto Update Mode:Version v%v
Auto Update Mode:None
Update Check Mode:Tags ^v[0-9.]+?$
Current Version:2.4.0.1
Current Version Code:73

