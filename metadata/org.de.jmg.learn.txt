Categories:Science & Education
License:GPLv3
Web Site:
Source Code:https://github.com/jhmgbl/learnforandroidfrag
Issue Tracker:https://github.com/jhmgbl/learnforandroidfrag/issues
Changelog:https://github.com/jhmgbl/learnforandroidfrag/releases

Auto Name:Learnforandroid
Summary:Learn vocabulary via flash cards
Description:
Enter and learn vocabulary. This is also suited for flash cards. The file format
is compatible with the Windows program learnforall. On long press your favorite
dictionary or Google Translate is shown. After a vocabulary has been shown the
user can press "view" and decide if he has known it or not (he can select
"right" or "wrong"). Long touch in settings shows a help screen.
.

Repo Type:git
Repo:https://github.com/jhmgbl/learnforandroidfragAS

#Repo:https://github.com/jhmgbl/learnforandroidfrag.git
Build:1.501,438
    disable=fix libraries
    commit=v1.501
    subdir=learnforAndroidFrag
    gradle=yes
    srclibs=AChartEngine@r534,3:AndroidFileChooser@0776630e9c5d3f73e66982c4214d5c1a64dfa15e,2:Support-v7@android-sdk-4.4.2_r1,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    rm=learnforAndroidFrag/libs/*
    build=pushd $$AChartEngine$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AChartEngine$$/target/achartengine-1.1.0.jar libs/

Auto Update Mode:None
#Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.515
Current Version Code:515

