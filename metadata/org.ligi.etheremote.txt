Categories:Security
License:GPLv3
Web Site:https://github.com/ligi/etheremote/blob/HEAD/README.md
Source Code:https://github.com/ligi/etheremote
Issue Tracker:https://github.com/ligi/etheremote/issues

Auto Name:ΞtheRemotΞ
Summary:Ethereum Remote
Description:
A Remote to access Ethereum nodes via JSON-RPC
.

Repo Type:git
Repo:https://github.com/ligi/etheremote

Build:0.1,1
    commit=0.1
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/android-sdk-manager/d' -e '/play-services/d' build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1

