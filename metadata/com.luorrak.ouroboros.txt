Categories:Internet
License:GPLv3
Web Site:https://github.com/Luorrak/Ouroboros/blob/HEAD/README.md
Source Code:https://github.com/Luorrak/Ouroboros
Issue Tracker:https://github.com/Luorrak/Ouroboros/issues

Auto Name:Ouroboros
Summary:Client for 8ch.net
Description:
Companion client for the 8chan/8ch.net image board. It's layout is very heavily
inspired by Chanobol and the design of Montanax's unreleased app Miko.
.

Repo Type:git
Repo:https://github.com/Luorrak/Ouroboros

Build:0.2.1,1
    commit=2bb94731a3193417fb2dace619fea62ff58ae88d
    subdir=app
    gradle=yes

Build:0.2.1,2
    commit=f86a46e8690bc789708eafcf6d580afca837b88f
    subdir=app
    gradle=yes

Build:0.3.1,4
    commit=f87633e89f4b53febc299092e8b3ff9aab3c3712
    subdir=app
    gradle=yes

Build:0.4.1,6
    commit=8955a6b3ca2744c9d9da6120b42cc7ff44506565
    subdir=app
    gradle=yes

Build:0.4.3,8
    commit=V0.4.3
    subdir=app
    gradle=yes

Build:0.5,9
    commit=V0.5
    subdir=app
    gradle=yes

Auto Update Mode:Version V%v
Update Check Mode:Tags
Current Version:0.5
Current Version Code:9

